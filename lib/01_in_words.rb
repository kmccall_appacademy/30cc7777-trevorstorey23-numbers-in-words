class Fixnum

  def in_words
    if self < 10
      ones(self)
    elsif self < 13
      ten_to_twelve(self)
    elsif self < 20
      teens(self)
    elsif self < 100
      tens(self)
    elsif self < 1000
      hundreds(self)
    elsif self < 1_000_000
      thousands(self)
    elsif self < 1_000_000_000
      millions(self)
    elsif self < 1_000_000_000_000
      billions(self)
    else
      trillions(self)
    end
  end

  def ones(int)
    case int
    when 0
      "zero"
    when 1
      "one"
    when 2
      "two"
    when 3
      "three"
    when 4
      "four"
    when 5
      "five"
    when 6
      "six"
    when 7
      "seven"
    when 8
      "eight"
    when 9
      "nine"
    end
  end

  def ten_to_twelve(int)
    case int
    when 10
      "ten"
    when 11
      "eleven"
    when 12
      "twelve"
    end
  end

  def teens(int)
    case int
    when 13
      "thirteen"
    when 14
      "fourteen"
    when 15
      "fifteen"
    when 16
      "sixteen"
    when 17
      "seventeen"
    when 18
      "eighteen"
    when 19
      "nineteen"
    end
  end

  def tens(int)
    case int
    when 20, 2
      "twenty"
    when 30, 3
      "thirty"
    when 40, 4
      "forty"
    when 50, 5
      "fifty"
    when 60, 6
      "sixty"
    when 70, 7
      "seventy"
    when 80, 8
      "eighty"
    when 90, 9
      "ninety"
    else
      less_than_100(int)
    end
  end

  def less_than_100(int)
    digits = int.to_s.split("").map { |e| e.to_i }
    "#{tens(digits[0])} #{ones(digits[1])}"
  end

  def hundreds(int)
    result = "#{(int / 100).in_words} hundred"
    if int % 100 != 0
      result += " #{(int%100).in_words}"
    end
    result
  end

  def thousands(int)
    result = "#{(int / 1000).in_words} thousand"
    if int % 1000 != 0
      result += " #{(int % 1000).in_words}"
    end
    result
  end

  def millions(int)
    result = "#{(int / 1_000_000).in_words} million"
    if int % 1_000_000 != 0
      result += " #{(int % 1_000_000).in_words}"
    end
    result
  end

  def billions(int)
    result = "#{(int / 1_000_000_000).in_words} billion"
    if int % 1_000_000_000 != 0
      result += " #{(int % 1_000_000_000).in_words}"
    end
    result
  end

  def trillions(int)
    result = "#{(int / 1_000_000_000_000).in_words} trillion"
    if int % 1_000_000_000_000 != 0
      result += " #{(int % 1_000_000_000_000).in_words}"
    end
    result
  end

end
